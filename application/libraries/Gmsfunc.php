<?php

class Gmsfunc {

    var $db;
    var $CI;
    var $daftar_bulan = array(
        1 => 'Januari',
        2 => 'Februari',
        3 => 'Maret',
        4 => 'April',
        5 => 'Mei',
        6 => 'Juni',
        7 => 'Juli',
        8 => 'Agustus',
        9 => 'September',
        10 => 'Oktober',
        11 => 'November',
        12 => 'Desember'
    );

    public function __construct() {
        $this->CI = & get_instance();
        $this->CI->load->database();
    }

    function headerData($page = 'home', $title = '') {
        $yMadeit = 2014;
        $yearMade = (date('Y') > $yMadeit) ? "$yMadeit - " . date('Y') : $yMadeit;
        $minified = ($this->CI->input->cookie('minified') == 'ok') ? 'minified' : '';
        $bdyhdnMenu = ($this->CI->input->cookie('hdnmenu') == 'ok') ? 'hidden-menu' : '';
        $subleft = 'Sebagai ';
        if ($this->CI->session->userdata('tsk_type') < 2) {
            $spanlogin = $this->CI->session->userdata('tsk_username');
            $subleft .= ($this->CI->session->userdata('tsk_type') == 0) ? 'Admin' : 'Sub Admin';
        } else {
            $spanlogin = $this->CI->session->userdata('tsk_username');
            $subleft .= 'Pegawai';
        }
        $imglogin = $this->CI->session->userdata('tsk_usrfoto'); //config_item('img') . 'default.jpg';
        $dtHeader = array(
            'no_main_header' => true,
            'page_title' => ucfirst($title),
            'page_css' => array(),
            'page_body_prop' => array("class" => "no-skin"),
            'yearMade' => $yearMade,
            'spanlogin' => $spanlogin,
            'subLeftTitle' => $subleft,
            'imglogin' => $imglogin,
            'hdr_name' => $this->CI->session->userdata('tsk_username'),
            'version' => $this->CI->config->item('version'),
        );
        return $dtHeader;
    }

    function useCurl($url, $isPost = false, $data = array(), $header = array(), $verbose = false) {
        if ($verbose) {
            ob_start();
            $out = fopen('php://output', 'w');
        }
        $ch = curl_init();
        curl_setopt_array($ch, array(
            CURLOPT_URL => $url,
            CURLOPT_RETURNTRANSFER => 1,
        ));
        if (count($header) > 0) {
            curl_setopt($ch, CURLOPT_HTTPHEADER, $header);
        }
        if ($isPost) {
            curl_setopt($ch, CURLOPT_POST, 1);
            curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($data));
        }
        if ($verbose) {
            curl_setopt($ch, CURLOPT_VERBOSE, true);
            curl_setopt($ch, CURLOPT_STDERR, $out);
        }
        $return = curl_exec($ch);        
        if ($verbose) {
            fclose($out);
            $debug = ob_get_clean();
            echo $debug;
        }
        curl_close($ch);
        return $return;
    }

    function getNewID($namatable, $column) {
        $this->CI->db->select_max($column);
        $max = $this->CI->db->get($namatable);
        if ($max->num_rows() > 0) {
            $row = $max->row_array();
            return $row[$column] + 1;
        } else {
            return 1;
        }
    }

    /**
     * Show Month's Names in Indonesian
     * @param Integer $index Start from 1 it's mean Januari
     * @return String  nama bulan in Indonesian
     */
    function getBulan($index) {
        return $this->daftar_bulan[$index - 1];
    }

    /**
     * Get number of month in a year, ex 1 for Januari.
     * @param String Month's name with first letter in uppercase, Month's name in indonesian ex: Januari
     * @return mixed if the word is found return number of The month, False Otherwise
     */
    function getBulanInNumber($namaBulan) {
        $nama = ucfirst($namaBulan);
        $s = array_search($nama, $this->daftar_bulan);
        $blnKe = ($s) ? $s + 1 : $s;
        return $blnKe;
    }

    /**
     * Mengembalikan string format tanggal.
     * @param type $date input tanggal type string dgn format dd mm yyyy Atau yyyy mm dd
     * @param type $splitChar karakter pemisah di tanggal
     * @param type $flip boolean, true untuk format dd mm yyyy -> yyyy mm dd (toDb) , false untuk format yyyy mm dd -> dd mm yyyy (from db)
     * @return string Tanggal
     */
    function format_tglSimpan($date, $splitChar = '-', $flip = TRUE) {
        $tt = '';
        if ($flip) {
            $tgl = explode($splitChar, $date);
            $tt = $tgl[2] . '-' . $tgl[1] . '-' . $tgl[0];
        } else {
            $tgl = explode('-', $date);
            $tt = $tgl[2] . $splitChar . $tgl[1] . $splitChar . $tgl[0];
        }
        return $tt;
    }

    /**
     * Fungsi ini hampir sama dengan fungsi dari php array_search(). Tapi dalam beberapa keadaan jika menggunakan fungsi tersebut beberapa data tidak tidak berfungsi.
     * @param String $text NIlai yang akan dicari di dalam array
     * @param Array $arr array satu dimensi
     * @return boolean Mengembalikan nilai TRUE jika data ditemukan. FALSE otherwise
     */
    function arr_search($text, $arr) {
        $rtn = false;
        foreach ($arr as $value) {
            if ($value == $text) {
                $rtn = true;
                break;
            }
        }
        return $rtn;
    }

    function volStandar($n, $periode) {
        $t = 0;
        if ($periode == 'hari') {
            $t = $n * 5 * 4 * 12; // 5 hari, 4 minggu, 12 bulan  => 240 hari efektif
        } else if ($periode == 'minggu') {
            $t = $n * 12 * 4; //* 12 bulan * 4 minggu
        } else if ($periode == 'bulan') {
            $t = $n * 12;     //* 12 bulan
        } else if ($periode == 'tahun') {
            $t = $n;
        } else {
            $t = NULL;
        }
        return $t;
    }

    function m2hy($m, $periode) {
        $h = 0;
        if ($periode == 'hari') {
            $h = ($m * 12 * 4 * 5) / 60; //* 12 bulan * 4 minggu * 5 hari
        } else if ($periode == 'minggu') {
            $h = ($m * 12 * 4) / 60;   //* 12 bulan * 4 minggu
        } else if ($periode == 'bulan') {
            $h = ($m * 12) / 60;     //* 12 bulan
        } else if ($periode == 'tahun') {
            $h = $m / 60;          //stahun
        } else {
            return null;
        }
        return $h;
    }

    function tomenit($num, $sat, $rule = 'BKN') {
        $efektif = ($rule == 'BKN') ? $this->EFEKTIF : $this->EFEKTIF_MEN;
        $out = '';
        if ($sat == 'menit') {
            $out = $num;
        } else if ($sat == 'jam') {
            $out = $num * 60;
        } else if ($sat == 'hari') {
            $out = $num * 60 * $efektif;
        } else if ($sat == 'minggu') {
            $out = $num * 60 * $efektif * 5;
        } else if ($sat == 'bulan') {
            $out = $num * 60 * $efektif * 5 * 4;
        } else {
            return null;
        }
        return $out;
    }

    //norma waktu 
    function menittostandar($n, $sat, $rule = 'BKN') {
        $efektif = ($rule == 'BKN') ? $this->EFEKTIF : $this->EFEKTIF_MEN;
        $out = '';
        if ($sat == 'menit') {
            $out = $n;
        } else if ($sat == 'jam') {
            $out = $n / 60;
        } else if ($sat == 'hari') {
            $out = $n / (60 * $efektif);
        } else if ($sat == 'minggu') {
            $out = $n / (60 * $efektif * 5);
        } else if ($sat == 'bulan') {
            $out = $n / (60 * $efektif * 5 * 4);
        } else {
            return null;
        }
        return $out;
    }

}
