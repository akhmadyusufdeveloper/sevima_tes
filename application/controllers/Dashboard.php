<?php

class Dashboard extends G_Controller {

    public function __construct() {
        parent::__construct();
    }

    public function index() {
        $header['user_img'] = $this->session->userdata('sos_usrfoto');
        $header['username'] = $this->session->userdata('sos_username');
        $header['namauser'] = $this->session->userdata('sos_namausr');
        $header['followers'] = $this->gData->get_data('following', array('id_user_following' => $this->session->userdata('sos_userid')))->num_rows();
        $header['disabled_follow'] = TRUE;
        $header['module'] = 'dashboard';
        $data['list_feed'] = $this->list_feed();

        $this->load->view('layout/header', $header);
        $this->load->view('page/dashboard', $data);

        $footer['page_js'] = $this->load->view('page_js/dashboard', '', TRUE);
        $this->load->view('layout/footer', $footer);
    }

    public function list_feed() {
        ob_start();

        $id_user = $this->session->userdata('sos_userid');
        $following = $this->gData->get_data('following', array('id_user' => $id_user));

        $get_following = array(intval($id_user));
        if ($following->num_rows() > 0) {
            foreach ($following->result_array() as $kf => $vf) {
                $get_following[] = intval($vf['id_user_following']);
            }
        }

        $arr_join_posts = array(array('t'=>'user u','j'=> 'u.id_user = p.id_user'));
        $posts = $this->gData->select_join('post p', $arr_join_posts, array(), "p.created_at DESC", FALSE)->where_in('p.id_user', $get_following)->limit(10, 0)->get();

        // $posts = $this->db->where_in('id_user', $get_following)->from('post')->limit(10, 0)->order_by('created_at DESC')->get();
        if ($posts->num_rows() > 0) {
            foreach ($posts->result_array() as $kp => $vp) {
                $like = $this->gData->get_data('like', array('id_post' => $vp['id_post']));

                $like_data = $like->result_array();
                $user_like = array_column($like_data, 'id_user');
                $icon_like = in_array(intval($id_user), $user_like) ? '<i class="fa fa-heart"></i>' : '<i class="ti-heart"></i>';

                $arr_join_comment = array(array('t'=>'user u','j'=> 'u.id_user = c.id_user'));
                $comment = $this->gData->select_join('comment c', $arr_join_comment, array('c.id_post' => $vp['id_post']), "c.created_at ASC");

                $btn_del_post = ($vp['id_user'] == $id_user) ? '<button class="btn btn-danger btn-del-post" data-id_post="'.$vp['id_post'].'"><i class="fa fa-trash"></i></button>' : '' ;

                $avatar_post = !empty($vp['image']) ? base_url('public/user/').$vp['image'] : base_url('assets/img/default.jpg');
                ?>
                <div class="central-meta item">
                    <div class="user-post">
                        <div class="friend-info">
                            <?= $btn_del_post ?>
                            <figure>
                                <img src="<?= $avatar_post ?>" alt="">
                            </figure>
                            <div class="friend-name">
                                <ins><a href="#" title=""><?= $vp['nama'] ?></a></ins>
                                <span>published: <?= $vp['created_at'] ?></span>
                            </div>
                            <div class="post-meta">
                                <img src="<?= base_url('public/post/').$vp['file_path'] ?>" alt="">
                                <div class="we-video-info">
                                    <ul>
                                        <li>
                                            <span class="comment" data-toggle="tooltip" title="Comments">
                                                <i class="fa fa-comments-o"></i>
                                                <ins><?= $comment->num_rows() ?></ins>
                                            </span>
                                        </li>
                                        <li>
                                            <span class="like" data-toggle="tooltip" title="like" data-id_post="<?= $vp['id_post'] ?>">
                                                <?= $icon_like ?>
                                                <ins><?= $like->num_rows() ?></ins>
                                            </span>
                                        </li>
                                    </ul>
                                </div>
                                <div class="description">
                                    <p>
                                        <?= $vp['caption'] ?>
                                    </p>
                                </div>
                            </div>
                        </div>
                        <div class="coment-area">
                            <ul class="we-comet">
                                <?php 
                                if ($comment->num_rows() > 0) { 
                                    foreach ($comment->result_array() as $kc => $vc) {
                                        $btn_del_comment = ($vc['id_user'] == $id_user) ? '<button class="btn btn-danger btn-sm btn-del-comment" data-id_comment="'.$vc['id_comment'].'"><i class="fa fa-trash"></i></button>' : '' ;
                                        $avatar_comment = !empty($vc['image']) ? base_url('public/user/').$vc['image'] : base_url('assets/img/default.jpg');
                                    ?>
                                    <li>
                                        <div class="comet-avatar">
                                            <img src="<?= $avatar_comment ?>" alt="">
                                        </div>
                                        <div class="we-comment">
                                            <div class="coment-head">
                                                <h5><a href="#" title=""><?= $vc['nama'] ?></a></h5>
                                                <span><?= $vc['created_at'] ?></span>
                                                <?= $btn_del_comment ?>
                                            </div>
                                            <p><?= $vc['comment'] ?></p>
                                        </div>
                                    </li>
                                    <?php } ?>
                                    <li>
                                        <a href="#" title="" class="showmore underline">more comments</a>
                                    </li>
                                    <?php
                                } else { ?>
                                    <li>
                                        <div class="comet-avatar"></div>
                                        <div class="we-comment">
                                            <p>Belum ada komentar</p>
                                        </div>
                                    </li>
                                <?php } ?>
                                
                                <li class="post-comment">
                                    <div class="comet-avatar">
                                        <img src="<?= $this->session->userdata('sos_usrfoto') ?>" alt="">
                                    </div>
                                    <div class="post-comt-box">
                                        <form class="comment-post">
                                            <input type="hidden" name="id_post" value="<?= $vp['id_post'] ?>">
                                            <textarea placeholder="Post your comment" name="comment"></textarea>
                                            <button type="button" class="btn-comment btn btn-primary" style="background-color: #0069d9;">kirim</button>
                                        </form>
                                    </div>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
                <?php
            }
        } else {
            ?>
            <div class="central-meta item">
                <div class="user-post">
                    <p>Tidak ada postingan</p>
                </div>
            </div>
            <?php
        }
        $tr = ob_get_clean();
        return $tr;
    }

    public function save_post() {
        $this->hanyaAjax();

        $this->load->library('form_validation');
        $this->form_validation->set_rules('caption', 'Caption', 'required|trim');
        $this->set_formMsg();
        $arr['sts'] = 0;
        if ($this->form_validation->run() == false) {
            $err = $this->form_validation->error_array();
            $this->pesan_error(reset($err));
        }

        $id_user = $this->session->userdata('sos_userid');

        $time_now = date("Y-m-d_H-i-s");
        $filename_uploaded = '';

        if (isset($_FILES['file_path']['name']) && $_FILES['file_path']['name'] != '') {
            $extension = array('jpg', 'jpeg', 'png');
            $uploaddir = 'public/post/';
            $fileName = basename($_FILES['file_path']['name']);
            $ext = strtolower(pathinfo($fileName, PATHINFO_EXTENSION));
            $fileNew = 'post_'.$time_now.'_'.$id_user. '.' . $ext;
            $fileDir = $uploaddir . $fileNew;

            if (is_numeric(array_search($ext, $extension)) and ( intval($_FILES['file_path']['size'] / 5120) <= 5120)) {
                if (move_uploaded_file($_FILES['file_path']['tmp_name'], $fileDir)) {
                    try {

                        $filename_uploaded = $fileNew;
                        $sts_upload = TRUE;
                    } catch (Exception $e) {
                        $arr['msg'] = "Terjadi kesalahan ketika menyimpan data gambar.";
                        $sts_upload = FALSE;
                    }
                } else {
                    $arr['msg'] = "Terjadi kesalahan ketika menyimpan data gambar.";
                    $sts_upload = FALSE;
                }
            } else {
                if (($_FILES['file_path']['size'] / 5120) > 5120) {
                    $arr['msg'] = "Data belum tersimpan.<br>Ukuran tidak boleh melebihi 5 Mb !";
                } else {
                    $arr['msg'] = "Data belum tersimpan.<br>File extensi yang diperbolehkan hanyalah jpg, jpeg dan png !";
                }
                $sts_upload = FALSE;
            }
        }

        if (!$sts_upload) {
            $arr['sts'] = 0;
            $this->echo_feedback($arr);
        }

        $data = array(
            'caption' => $this->input->post('caption'),
            'id_user' => $id_user,
            'file_path' => $filename_uploaded,
            'created_at' => date("Y-m-d H:i:s")
        );
        $s = $this->gData->save_data('post', $data);

        if (!$s) {
            $arr['sts'] = 0;
            $arr['msg'] = "Data belum tersimpan.<br>Ada kesalahan ketika menyimpan data !";
        } else {
            $arr['sts'] = 1;
            $arr['msg'] = 'Data telah disimpan';
            $arr['list_feed'] = $this->list_feed();
        }

        $this->echo_feedback($arr);
    }

    public function save_comment() {
        $this->hanyaAjax();

        $this->load->library('form_validation');
        $this->form_validation->set_rules('comment', 'Komentar', 'required|trim');
        $this->set_formMsg();
        $arr['sts'] = 0;
        if ($this->form_validation->run() == false) {
            $err = $this->form_validation->error_array();
            $this->pesan_error(reset($err));
        }

        $id_user = $this->session->userdata('sos_userid');

        $data = array(
            'id_post' => $this->input->post('id_post'),
            'comment' => $this->input->post('comment'),
            'id_user' => $id_user,
            'created_at' => date("Y-m-d H:i:s")
        );
        $s = $this->gData->save_data('comment', $data);

        if (!$s) {
            $arr['sts'] = 0;
            $arr['msg'] = "Data belum tersimpan.<br>Ada kesalahan ketika menyimpan data !";
        } else {
            $arr['sts'] = 1;
            $arr['msg'] = 'Data telah disimpan';
            $arr['list_feed'] = $this->list_feed();
        }

        $this->echo_feedback($arr);
    }

    public function like($id_post) {
        $this->hanyaAjax();

        $id_user = $this->session->userdata('sos_userid');

        $cek_like = $this->gData->get_data('like', array('id_post' => $id_post, 'id_user' => $id_user));
        if ($cek_like->num_rows() > 0) {
            $like_data = $cek_like->row_array();
            $s = $this->gData->delete_data('like', array('id_like' => $like_data['id_like']));
            $like = FALSE;
        } else {
            $data = array(
                'id_post' => $id_post,
                'id_user' => $id_user
            );
            $s = $this->gData->save_data('like', $data);
            $like = TRUE;
        }

        if (!$s) {
            $arr['sts'] = 0;
            $arr['msg'] = "Data belum tersimpan.<br>Ada kesalahan ketika menyimpan data !";
        } else {
            $arr['sts'] = 1;
            $arr['msg'] = 'Data telah disimpan';
            $arr['icon_like'] = ($like) ? "fa fa-heart" : "ti-heart";

            $like_data = $this->gData->get_data('like', array('id_post' => $id_post));
            $arr['count_like'] = $like_data->num_rows();
        }

        $this->echo_feedback($arr);
    }

    public function del_post($id_post) {
        $this->hanyaAjax();

        $s = $this->gData->delete_data('post', array('id_post' => $id_post));
        if (!$s) {
            $arr['sts'] = 0;
            $arr['msg'] = "Data belum terhapus.<br>Ada kesalahan ketika menyimpan data !";
        } else {
            $arr['sts'] = 1;
            $arr['msg'] = 'Data telah dihapus';
            $arr['list_feed'] = $this->list_feed();
        }

        $this->echo_feedback($arr);
    }

    public function del_comment($id_comment) {
        $this->hanyaAjax();

        $s = $this->gData->delete_data('comment', array('id_comment' => $id_comment));
        if (!$s) {
            $arr['sts'] = 0;
            $arr['msg'] = "Data belum terhapus.<br>Ada kesalahan ketika menyimpan data !";
        } else {
            $arr['sts'] = 1;
            $arr['msg'] = 'Data telah dihapus';
            $arr['list_feed'] = $this->list_feed();
        }

        $this->echo_feedback($arr);
    }

    public function profil_image() {
        $this->hanyaAjax();

        $id_user = $this->session->userdata('sos_userid');

        if (isset($_FILES['img-profil']['name']) && $_FILES['img-profil']['name'] != '') {
            $extension = array('jpg', 'jpeg', 'png');
            $uploaddir = 'public/user/';
            $fileName = basename($_FILES['img-profil']['name']);
            $ext = strtolower(pathinfo($fileName, PATHINFO_EXTENSION));
            $fileNew = 'profil_'.$id_user. '.' . $ext;
            $fileDir = $uploaddir . $fileNew;

            if (is_numeric(array_search($ext, $extension)) and ( intval($_FILES['img-profil']['size'] / 5120) <= 5120)) {
                if (move_uploaded_file($_FILES['img-profil']['tmp_name'], $fileDir)) {
                    try {

                        $filename_uploaded = $fileNew;
                        $sts_upload = TRUE;
                    } catch (Exception $e) {
                        $arr['msg'] = "Terjadi kesalahan ketika menyimpan data gambar.";
                        $sts_upload = FALSE;
                    }
                } else {
                    $arr['msg'] = "Terjadi kesalahan ketika menyimpan data gambar.";
                    $sts_upload = FALSE;
                }
            } else {
                if (($_FILES['img-profil']['size'] / 5120) > 5120) {
                    $arr['msg'] = "Data belum tersimpan.<br>Ukuran tidak boleh melebihi 5 Mb !";
                } else {
                    $arr['msg'] = "Data belum tersimpan.<br>File extensi yang diperbolehkan hanyalah jpg, jpeg dan png !";
                }
                $sts_upload = FALSE;
            }
        }

        if (!$sts_upload) {
            $arr['sts'] = 0;
            $this->echo_feedback($arr);
        }

        $data = array('image' => $filename_uploaded);
        $s = $this->gData->save_data('user', $data, TRUE, array('id_user' => $id_user));

        if (!$s) {
            $arr['sts'] = 0;
            $arr['msg'] = "Data belum tersimpan.<br>Ada kesalahan ketika menyimpan data !";
        } else {
            $arr['sts'] = 1;
            $arr['msg'] = 'Data telah disimpan';

            $this->session->unset_userdata('sos_usrfoto');

            $new_path_profil_image = base_url('public/user/').$filename_uploaded;
            $this->session->set_userdata('sos_usrfoto', $new_path_profil_image);
        }

        $this->echo_feedback($arr);
    }

}
