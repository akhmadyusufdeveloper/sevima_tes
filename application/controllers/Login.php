<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends G_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->library('form_validation');
    }

    public function index() {
        $this->load->view('login');
    }

    public function doLogin() {
        $this->hanyaAjax();
        $curl_login = $this->gmsfunc->useCurl(base_url('api/user/login'), TRUE, $this->input->post());
        $result = json_decode($curl_login, TRUE);

        $curl_profil = $this->gmsfunc->useCurl(base_url('api/user/profil?id_user='.$result['data']['id_user']), FALSE, array(), array('Authorization: '. $result['data']['token']));
        $result2 = json_decode($curl_profil, TRUE);

        $echo = array('ind' => 0, 'msg' => 'User dan Password tidak Cocok');
        if ($result['status']) {
            // $set_session = array(
            //     'id_user' => $result['data']['id_user'],
            //     'token' => $result['data']['token'],
            //     'username' => $result2['data']['username'],
            //     'nama' => $result2['data']['nama'],
            //     'image_profil' => $result2['data']['image_profil']
            // );
            // $this->setSession($set_session);

            $datasession = array(
                'sos_userid' =>  $result['data']['id_user'],
                'sos_token' => $result['data']['token'],
                'sos_username' => $result2['data']['username'],
                'sos_usrfoto' => $result2['data']['image_profil'],
                'sos_namausr' => $result2['data']['nama'],
            );
            $this->session->set_userdata($datasession);

            $echo = array('ind' => 1, 'msg' => 'Login Berhasil');
            
        }

        $this->output->set_header('Content-type:Application/json');
        echo json_encode($echo);
        exit();
    }

    public function doSignUp() {
        $this->hanyaAjax();
        $curl_register = $this->gmsfunc->useCurl(base_url('api/user/register'), TRUE, $this->input->post());
        $result = json_decode($curl_register, TRUE);

        $echo = array('ind' => 0, 'msg' => $result['message']);
        if ($result['status']) {
            $echo = array('ind' => 1, 'msg' => $result['message']);
        }

        $this->output->set_header('Content-type:Application/json');
        echo json_encode($echo);
        exit();
    }

//     function isStillLogin() {
// //        if (isset($_SESSION['LAST_ACTIVITY']) && ((time() - $_SESSION['LAST_ACTIVITY']) > 1800)) {
// //            $this->session->sess_destroy();
// //        }
// //        $_SESSION['LAST_ACTIVITY'] = time();
//         if ($this->session->has_userdata('sos_username')) {
//             echo 'in';
//         } else {
//             $this->log_lgn('lgt');
//             echo 'out';
//         }
//     }

    function logout() {
        $this->log_lgn('lgt');
        session_destroy();
        redirect('login', 'refresh');
    }

    function modalprofil_() {
        ob_start();
        $this->createModal('mdlprofiluser', 'Profil');
        $src = $this->session->userdata('spg_usrfoto');
        ?>
        <div class="form-group">
            <div class="col-md-8">
                <div class="form-group">
                    <label class="col-md-4 control-label">Username</label>
                    <div class="col-md-8">
                        <input id="usr" name="usr" class="form-control input-sm" type="text" value="" autocomplete="off">
                        <small class="text-muted">Boleh kosong jika tidak ingin mengganti username.</small>
                    </div>                     
                </div>
                <div class="form-group">
                    <label class="col-md-4 control-label">Password</label>
                    <div class="col-md-8">
                        <input id="pwd" name="pwd" class="form-control input-sm" type="password" />
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-md-4 control-label">Tulis Ulang Password</label>
                    <div class="col-md-8">
                        <input id="repwd" placeholder="Tulis ulang password" name="repwd" class="form-control input-sm" type="password" />
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-md-4 control-label">Password lama</label>
                    <div class="col-md-8">
                        <input id="oldpwd" placeholder="Password Lama" name="oldpwd" class="form-control input-sm" type="password" />
                    </div>
                </div>
            </div>
            <?php if ($this->session->userdata('spg_type') < 2) { ?>
                <div class="col-md-4 text-center">
                    <img style="height: 125px;width: 115px; margin-bottom: 10px;display: inline;" class="thumbnail" src="<?= $src ?>" id="preview"><br>
                    <input type="file" name="foto" id="foto" class="btn btn-default btn-sm" style="width: 100%">                    
                    <small class="pull-left">Size max : 500kb</small>
                </div>                                                
            <?php } ?>
        </div> 
        <?php
        $mdlBodyContent = ob_get_contents();
        ob_clean();
        $this->createModalBody($mdlBodyContent);
        $tr['mdl'] = $this->getModal();
        echo json_encode($tr);
    }

    function cekOldpass($str) {
        $id = $this->session->userdata('spg_userid');
        if ($this->session->userdata('spg_type') == 2) {
            $d = $this->gData->get_data('spg_pegawai', array('peg_id' => $id), '', FALSE)
                            ->select('peg_password', 'pass')->get()->row();
        } else {
            $d = $this->gData->get_data('spg_asdf', array('as_user_id' => $id), '', FALSE)
                            ->select('as_password', 'pass')->get()->row();
        }
        if ($d->pass == $str) {
            return TRUE;
        } else {
            $this->form_validation->set_message('cekOldpass', '<strong>{field}</strong> yang diinputkan salah.');
            return FALSE;
        }
    }

    function saveprofil_() {
        $this->hanyaAjax();
        $this->load->library('form_validation');
        $this->form_validation->set_rules('usr', 'Username', "trim");
        $this->form_validation->set_rules('pwd', 'Password', 'trim');
        $this->form_validation->set_rules('repwd', 'Kolom Tulis Ulang Password', 'matches[pwd]');
        $this->form_validation->set_rules('oldpwd', 'Password Lama', 'callback_cekOldpass');
        $this->set_formMsg(array(
            'matches' => '<strong>{field}</strong> Tidak Sama.'
        ));
        if (!$this->form_validation->run()) {
            $this->input_error();
        }
        $id = $this->session->userdata('spg_userid');
        if ($this->session->userdata('spg_type') == 2) {
            if ($this->input->post('pwd')) {
                $data = array(
                    'peg_password' => $this->input->post('nama')
                );
            }
            if ($this->input->post('usr')) {
                $data['peg_username'] = $this->input->post('usr');
            }
        } else {
            if ($this->input->post('pwd')) {
                $data = array(
                    'as_password' => $this->input->post('nama')
                );
            }
            if ($this->input->post('usr')) {
                $data['as_username'] = $this->input->post('usr');
            }
        }
        $cekUpload = FALSE;
        if ($_FILES['foto']['name'] != '') {
            $config['upload_path'] = './assets/img/app';
            $config['allowed_types'] = 'gif|jpg|png';
            $config['max_size'] = 500;
            $config['file_ext_tolower'] = TRUE;
            $config['file_name'] = 'usr_' . $this->session->userdata('spg_type') . '_' . $id;
            $this->load->library('upload', $config);
            if (!$this->upload->do_upload('foto')) {
                $err = $this->upload->display_errors();
                $this->pesan_error($err);
            } else {
                $cekUpload = TRUE;
                $keyFoto = $this->session->userdata('spg_type') == 2 ? 'peg_foto' : 'as_foto';
                $data[$keyFoto] = $this->upload->data('file_name');
            }
        }

        if ($this->session->userdata('spg_type') == 2) {
            $opt['tbl'] = 'spg_pegawai';
            $opt['id'] = 'peg_id';
        } else {
            $opt['tbl'] = 'spg_asdf';
            $opt['id'] = 'as_user_id';
        }
        $arr['reload'] = 'no';
        if (count($data) > 0) {
            $ss = $this->gData->save_data($opt['tbl'], $data, TRUE, array($opt['id'] => $id));
            if ($ss) {
                $arr['sts'] = 1;
                $arr['msg'] = 'Data berhasil disimpan';
                $arr['reload'] = 'ok';
                if ($cekUpload) {
                    $urImg = config_item('img') . 'app/' . $data[$keyFoto] . '?' . microtime();
                    $this->session->set_userdata('spg_usrfoto', $urImg);
                }
                if ($this->input->post('usr')) {
                    $this->session->set_userdata('spg_username', $this->input->post('usr'));
                }
            } else {
                $arr['sts'] = 0;
                $arr['msg'] = 'Data gagal disimpan';
            }
        } else {
            $arr['sts'] = 0;
            $arr['msg'] = 'Tidak ada data yang disimpan';
        }
        $this->echo_feedback($arr);
    }

}
