<?php

class Friends extends G_Controller {

    public function __construct() {
        parent::__construct();
    }

    public function index() {
        $header['user_img'] = $this->session->userdata('sos_usrfoto');
        $header['username'] = $this->session->userdata('sos_username');
        $header['namauser'] = $this->session->userdata('sos_namausr');
        $header['followers'] = $this->gData->get_data('following', array('id_user_following' => $this->session->userdata('sos_userid')))->num_rows();
        $header['disabled_follow'] = TRUE;
        $header['module'] = 'friends';
        $data['list_follow'] = $this->list_follow();

        $this->load->view('layout/header', $header);
        $this->load->view('page/friends', $data);

        $footer['page_js'] = $this->load->view('page_js/friends', '', TRUE);
        $this->load->view('layout/footer', $footer);
    }

    public function list_follow() {
        ob_start();

        $id_user = $this->session->userdata('sos_userid');

        // $arr_join_following = array(array('t'=>'user u','j'=> 'u.id_user = f.id_user'));
        // $following = $this->gData->select_join('following f', $arr_join_following, array('f.id_user' => $id_user), "u.nama ASC");

        $following = $this->gData->get_data('following', array('id_user' => $id_user));
        if ($following->num_rows() > 0) {
            $data_following = array_column($following->result_array(), 'id_user_following');
            $data_user = $this->db->where_in('id_user', $data_following)->from('user')->order_by('nama ASC')->get();
            foreach ($data_user->result_array() as $kf => $vf) {
                $avatar = !empty($vf['image']) ? base_url('public/user/').$vf['image'] : base_url('assets/img/default.jpg');
                $btn_follow = (in_array($vf['id_user'], $data_following)) ? '<a href="#" title="" class="add-butn more-action btn-friend" data-action="unfollow" data-id_user="'.$vf['id_user'].'" data-ripple="">unfollow</a>' : '<a href="#" title="" class="add-butn btn-friend" data-action="follow" data-id_user="'.$vf['id_user'].'" data-ripple="">follow</a>';
                ?>
                <li>
                    <div class="nearly-pepls">
                        <figure>
                            <a href="time-line.html" title=""><img src="<?= $avatar ?>" alt=""></a>
                        </figure>
                        <div class="pepl-info">
                            <h4><a href="time-line.html" title=""><?= $vf['nama'] ?></a></h4>
                            <span><?= $vf['username'] ?></span>
                            <?= $btn_follow ?>
                        </div>
                    </div>
                </li>
                <?php
            }
        } else {
            ?>
            <p>Tidak ada following</p>
            <?php
        }
        $tr = ob_get_clean();
        return $tr;
    }

    public function cari() {
        $this->hanyaAjax();
        
        $value_search = $this->input->post('value_search');
        $this->db->like('username', $value_search, 'both');
        $this->db->like('nama', $value_search, 'both');
        $this->db->from('user');
        $users = $this->db->get();

        ob_start();
        if ($users->num_rows() > 0 && !empty($value_search)) {

            $id_user = $this->session->userdata('sos_userid');
            $following = $this->gData->get_data('following', array('id_user' => $id_user))->result_array();
            $data_following = array_column($following, 'id_user_following');

            foreach ($users->result_array() as $kf => $vf) {
                if ($vf['id_user'] != $id_user) {
                    $avatar = !empty($vf['image']) ? base_url('public/user/').$vf['image'] : base_url('assets/img/default.jpg');
                    $btn_follow = (in_array($vf['id_user'], $data_following)) ? '<a href="#" title="" class="add-butn more-action btn-friend" data-action="unfollow" data-id_user="'.$vf['id_user'].'" data-ripple="">unfollow</a>' : '<a href="#" title="" class="add-butn btn-friend" data-action="follow" data-id_user="'.$vf['id_user'].'" data-ripple="">follow</a>';
                    ?>
                    <li>
                        <div class="nearly-pepls">
                            <figure>
                                <a href="time-line.html" title=""><img src="<?= $avatar ?>" alt=""></a>
                            </figure>
                            <div class="pepl-info">
                                <h4><a href="time-line.html" title=""><?= $vf['nama'] ?></a></h4>
                                <span><?= $vf['username'] ?></span>
                                <?= $btn_follow ?>
                            </div>
                        </div>
                    </li>
                    <?php
                }
            }
        } else {
            ?>
            <p>Tidak ditemukan teman dengan keyword yang dicari</p>
            <?php
        }
        $list_search = ob_get_contents();
        ob_clean();

        $tr['list_search'] = $list_search;
        echo json_encode($tr);
    }

    public function follow($id_user_follow, $action) {
        $this->hanyaAjax();

        $id_user = $this->session->userdata('sos_userid');

        if ($action == 'unfollow') {
            $s = $this->gData->delete_data('following', array('id_user' => $id_user, 'id_user_following' => $id_user_follow));
        } else {
            $data = array('id_user' => $id_user, 'id_user_following' => $id_user_follow);
            $s = $this->gData->save_data('following', $data);
        }

        if (!$s) {
            $arr['sts'] = 0;
            $arr['msg'] = "Data belum tersimpan.<br>Ada kesalahan ketika menyimpan data !";
        } else {
            $arr['sts'] = 1;
            $arr['msg'] = 'Data telah disimpan';
        }
        $this->echo_feedback($arr);
    }

}
