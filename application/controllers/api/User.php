<?php

defined('BASEPATH') or exit('No direct script access allowed');

require APPPATH . '/libraries/REST_Controller.php';
require APPPATH . '/libraries/Format.php';

class User extends REST_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->library('form_validation');
        $this->load->library('session');
        $this->load->model('M_user');
    }

    private function setHeader()
    {
        header('HTTP/1.1 500 OK');
        header("Content-Type: application/json");
    }

    private function _formvalidationmsg($arrMsg = array())
    {
        $default = array(
            'required' => '{field} harus diisi.',
            'greater_than' => '{field} harus ditentukan terlebih dahulu.',
            'max_length' => 'Maksimal karakter yang diperbolehkan untuk kolom {field} sebesar {param} karakter.',
            'numeric' => 'Hanya angka yang diperbolehkan untuk kolom {field}',
            'is_unique' => 'Maaf value input pada {field} telah digunakan, silahkan gunakan value lain',
            'matches' => '{field} tidak sama'
        );
        $setArr = array_merge($default, $arrMsg);
        foreach ($setArr as $key => $msg) {
            $this->form_validation->set_message($key, $msg);
        }
    }

    private function input_error()
    {
        $json['status'] = FALSE;
        $json['code'] = self::HTTP_BAD_REQUEST;
        $json['message'] = strip_tags(validation_errors());
        echo json_encode($json);
        exit();
    }

    function index_get() {
        
        exit();
    }

    public function register_post() {
        $username = trim($this->post('username'));

        // $cek_user = $this->M_user->get_user($username);

        if (!empty($username)) {
            $this->form_validation->set_rules('username', 'Username', 'required|max_length[20]|is_unique[user.username]');
        } else {
            $this->form_validation->set_rules('username', 'Username', 'required|max_length[20]');
        }

        $this->form_validation->set_rules('nama', 'Nama', 'required|max_length[50]|trim');
        $this->form_validation->set_rules('password', 'Password', 'required|max_length[50]|trim');
        // $this->form_validation->set_rules('confirm_password', 'Konfirm Password', 'required|matches[password]');

        $this->_formvalidationmsg();
        if (!$this->form_validation->run()) {
            $this->input_error();
        }

        $result = array(
            'status' => true,
            'message' => 'Berhasil Mendaftar, silahkan menuju login',
        );
        $data = $this->post();
        // unset($data['confirm_password']);
        $register = $this->M_user->register($data);
        if (!$register) {
            $result = array(
                'status' => false,
                'message' => 'Gagal Mendaftar',
            );
        }

        $this->set_response($result, REST_Controller::HTTP_OK);
    }

    public function update_post() {
        $username = trim($this->post('username'));

        $cek_user = $this->M_user->get_user($username);

        if ($username != $cek_user['username'] && !empty($username)) {
            $this->form_validation->set_rules('username', 'Username', 'required|max_length[20]|is_unique[user.username]');
        } else {
            $this->form_validation->set_rules('username', 'Username', 'required|max_length[20]');
        }

        $this->form_validation->set_rules('nama', 'Nama', 'required|max_length[50]|trim');
        $this->form_validation->set_rules('password', 'Password', 'required|max_length[50]|trim');
        $this->form_validation->set_rules('confirm_password', 'Konfirm Password', 'required|matches[password]');

        $this->_formvalidationmsg();
        if (!$this->form_validation->run()) {
            $this->input_error();
        }

        $result = array(
            'status' => true,
            'message' => 'Berhasil Memperbarui',
        );

        $id_user = end($this->uri->segment_array());
        $data = $this->post();
        unset($data['confirm_password']);
        $update = $this->M_user->update($data, $id_user);
        if (!$update) {
            $result = array(
                'status' => false,
                'message' => 'Gagal Memperbarui',
            );
        }

        $this->set_response($result, REST_Controller::HTTP_OK);
    }

    /**
     * Login pns
     * method login 
     * type get param usrname, psword
     * return JSON (success)
     *      status, message
     *          on success [status = 1] id(idpns), nip, nama, token
     */
    public function login_post()
    {
        $result = $this->M_user->authentication($this->post('username'), $this->post('password'));
        if ($result['status']) {

            $dttoken = array(
                'id' => $result['data']['id_user']
            );

            // generate token baru
            $token = AUTHORIZATION::generateToken($dttoken);
            $result['data']['token'] = $token;
            $register = $this->M_user->token_register($dttoken['id'], $token);
            if (!$register) {
                $result = array(
                    'status' => false,
                    'message' => 'Gagal menyimpan token'
                );
            }
        }

        $this->set_response($result, REST_Controller::HTTP_OK);
    }

    function auth()
    {
        if (isset($_SERVER['PHP_AUTH_USER'])) {
            if (!($this->uAuth == $_SERVER['PHP_AUTH_USER'] && $this->pAuth == $_SERVER['PHP_AUTH_PW'])) {
                $result = array(
                    'status' => false,
                    'message' => 'Authentication failed, something missing in your Request',
                );
                $this->set_response($result, REST_Controller::HTTP_OK);
            }
        } else {
            $result = array(
                'status' => false,
                'message' => 'App Not Found!!',
            );
            $this->set_response($result, REST_Controller::HTTP_OK);
        }
        return true;
    }

    /**
     * Digunakan untuk mendaftarkan pelapor
     * method daftar_post 
     * type POST parameter:  opd_id, nama, jabatan, no_telp, alamat, email, pelapor_usr, pelapor_pass
     */
    public function edit_profil_post()
    {
        $headers = $this->input->request_headers();
        $result = $this->token_isactive($headers);

        $old_data = $this->ProfilMdl->get_pns($this->input->post('nip'));

        if ($result['status']) {
            $this->form_validation->set_rules('no_telp', 'Nomor Telepon', 'required|numeric|max_length[30]');

            if ($this->input->post('email') != $old_data->email) {
                $this->form_validation->set_rules('email', 'Email', 'required|valid_email|max_length[150]|is_unique[skp_pns.email]');
            } else {
                $this->form_validation->set_rules('email', 'Email', 'required|valid_email|max_length[150]');
            }

            $this->_formvalidationmsg();
            if (!$this->form_validation->run()) {
                $this->input_error();
            }

            $data = array(
                'notelp' => $this->input->post('no_telp'),
                'email' => $this->input->post('email'),
            );

            $ext = pathinfo($_FILES['file']['name'], PATHINFO_EXTENSION);
            if (!empty($_FILES)) {
                $tempFile = $_FILES['file']['tmp_name'];
                $fileName = 'pf_' . $this->input->post('nip') . '_.' . $ext;
                $targetPath = getcwd() . '/img_pns/';
                $targetFile = $targetPath . $fileName;
                if (move_uploaded_file($tempFile, $targetFile)) {
                    $data['image_profil'] = $fileName;
                }
            }

            $q_save = $this->ProfilMdl->updatePns($this->input->post('nip'), $data);

            if ($q_save) {
                $result['status'] = TRUE;
                $result['message'] = 'Data Profil berhasil diperbarui';
            } else {
                $result['status'] = FALSE;
                $result['message'] = 'Data Profil gagal diperbarui';
            }
        }

        $this->set_response($result, REST_Controller::HTTP_OK);
    }

    /**
     * Get Profil
     * method profil 
     * type get id 
     * return JSON (success)
     *      status, message 
     *          status == 1
     */
    public function profil_get()
    {
        $headers = $this->input->request_headers();
        $result = $this->token_isactive($headers);
        if ($result['status']) {
            $result['data'] = $this->M_user->get_data_user($this->get('id_user'));
            // unset($result['data']['username']);
            unset($result['data']['password']);
            unset($result['data']['token']);
            unset($result['data']['token_expired']);
            unset($result['data']['id_user']);

            $img_pns = base_url('public/user') . '/' . $result['data']['image_profil'];
            $result['data']['image_profil'] = file_exists($img_pns) ? $img_pns : base_url('assets/img/default.jpg');
        }
        $this->set_response($result, REST_Controller::HTTP_OK);
    }

    /**
     * Ubah password
     * method changepassword 
     * type POST param id, oldpsword, newpsword
     * return JSON (success)
     *      status, message
     *          on error [status = 0] code(error code DB),message(message error DB transaction)
     */
    public function edit_password_post()
    {
        $headers = $this->input->request_headers();
        $result = $this->token_isactive($headers);

        // $this->form_validation->set_rules('id', 'PNS ID', 'required');
        $this->form_validation->set_rules('old_password', 'Password Lama', 'required|max_length[50]');
        $this->form_validation->set_rules('new_password', 'Password Baru', 'required|max_length[50]');
        $this->form_validation->set_rules('renew_password', 'Password Baru (Konfirmasi)', 'required|matches[new_password]');
        $setrules = array(
            'matches' => '{field} Tidak cocok'
        );
        $this->_formvalidationmsg($setrules);
        if (!$this->form_validation->run()) {
            $this->input_error();
        }
        if ($result['status']) {
            $result = $this->Usermodel->changepass($this->input->post('nip'), $this->input->post('old_password'), $this->input->post('new_password'));
        }
        $this->set_response($result, REST_Controller::HTTP_OK);
    }

    /**
     * Ubah profil image
     * method profil_img_upload_post
     * type POST param id, profil_img
     * return JSON (success)
     *      status, message
     *          on error [status = 0] code(error code DB),message(message error DB transaction)
     */
    public function profil_img_upload_post() {
        $this->load->library('upload');
        
        $ext = pathinfo($_FILES['profil_img']['name'], PATHINFO_EXTENSION);
        $fileName = 'pf_' . $this->post('id_user') . '_.' . $ext;

        $config['upload_path'] = getcwd() . '/public/user/'; //path folder
        $config['allowed_types'] = 'jpg|png|jpeg'; //type yang dapat diakses bisa anda sesuaikan
        $config['file_name'] = $fileName; //Enkripsi nama yang terupload
        $config['overwrite'] = TRUE; 
        $headers = $this->input->request_headers();
        $result = $this->token_isactive($headers);
        if ($result['status']) {
            $this->upload->initialize($config);
            if (!empty($_FILES['profil_img']['name'])) {
                if ($this->upload->do_upload('profil_img')) {
                    $data['image_profil'] = $fileName;
                    $q_save = $this->M_user->upate($this->post('id_user'), $data);
                    if ($q_save) {
                        $result['status'] = TRUE;
                        $result['message'] = 'Data Profil berhasil diperbarui';
                    } else {
                        $result['status'] = FALSE;
                        $result['message'] = 'Data Profil gagal diperbarui';
                    }
                }
            } else {
                $result = array(
                    'status' => false,
                    'message' => 'Tidak ada gambar yang diupload'
                );
            }
            $result['code'] = self::HTTP_OK;
        }
        $this->set_response($result, self::HTTP_OK);
    }
}
