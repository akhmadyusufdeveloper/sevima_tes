<?php

class G_Controller extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->session_cek();
    }

    function input_error() {
        $json['sts'] = 0;
        $json['msg'] = "<div class='alert alert-warning error_validasi'>" . validation_errors() . "</div>";
        echo json_encode($json);
        exit();
    }

    function pesan_error($pesan = "Terjadi kesalahan, coba lagi !") {
        $json['sts'] = 0;
        $json['msg'] = $pesan;
        echo json_encode($json);
        exit();
    }

    function pesan_akses($option) {
        if (!$this->priv_user($option)) {
            $this->pesan_error('Maaf anda tidak memiliki hak akses untuk menambah data');
        }
    }

    function echo_feedback($pesan) {
        $json['sts'] = 0;
        $json['msg'] = $pesan;
        if (is_array($pesan)) {
            $json = $pesan;
        }
        echo json_encode($json);
        exit();
    }

    function tdk_sama($str, $val) {
        if ($str == $val) {
            $this->form_validation->set_message('tdk_sama', "<strong>{field}</strong> harus ditentukan terlebih dahulu.");
            return FALSE;
        } else {
            return TRUE;
        }
    }

    function set_formMsg($arrMsg = array()) {
        $default = array(
            'required' => '<strong>{field}</strong> wajib diisi. <strong>Tidak Boleh Kosong</strong>.',
            'greater_than' => '<strong>{field}</strong> harus ditentukan terlebih dahulu.',
            'max_length' => 'Maksimal karakter yang diperbolehkan untuk kolom <strong>{field}</strong> sebesar {param} karakter.'
        );
        $setArr = array_merge($default, $arrMsg);
        foreach ($setArr as $key => $msg) {
            $this->form_validation->set_message($key, $msg);
        }
    }

    function hanyaAjax() {
        if (!$this->input->is_ajax_request()) {
            show_error('No Direct Script Access Allowed');
        }
    }

    function session_cek() {
        $controller = $this->router->fetch_class();
        $method = $this->router->fetch_method();
        if (strtolower($controller) == 'login') {
            $pageAfterLogin = base_url('dashboard');
            $aksesAllUser = array('logout', 'modalprofil_', 'saveprofil_');
            if ($this->session->has_userdata('sos_userid') && !in_array($method, $aksesAllUser)) {
                redirect($pageAfterLogin);
            }
        } else {
            if (!$this->session->has_userdata('sos_userid')) {
                $this->session->set_userdata('tsk_reload_uri', $this->uri->uri_string());
                redirect(base_url());
            }
            
        }
    }

    function clean_tag_input($str) {
        $t = preg_replace('/<[^<|>]+?>/', '', htmlspecialchars_decode($str));
        $t = htmlentities($t, ENT_QUOTES, "UTF-8");
        $t = trim($t);
        return $t;
    }

    var $modal;

    /**
     * Bikin div modal header 
     * @param String  $attrId Atribut ID untuk div modal. Dan Form dengan prefix id ='fo-' 
     * @param String $title
     */
    function createModal($attrId = '', $title = '', $islarge = FALSE) {
        $lg = ($islarge) ? 'modal-lg' : '';
        $this->modal = '<div id="' . $attrId . '" class="modal fade in" aria-hidden="false">
    <div class="modal-dialog ' . $lg . '">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h3 class="modal-title">' . $title . '</h3>
            </div>
            <form id="fo-' . $attrId . '" class="form-horizontal">
                <div class="modal-body">';
    }

    /**
     * Isi content dari modal yg dibikin dgn fungsi createModal
     * @param String $content html content body
     */
    function createModalBody($content) {
        if (is_null($this->modal)) {
            show_error('Function CreateModal not Execution yet. Modal is Empty');
        }
        $this->modal .= $content;
    }

    /**
     * Mendapatkan modal dalam format HTML 
     * @param Array $attrBtnSave atribut untuk tombol Simpan, tag Button. Default : <pre>array('type' => "submit")</pre>
     * @return String html modal 
     */
    function getModal($attrBtnSave = array('type' => "submit")) {
        if (is_null($this->modal)) {
            show_error('Function CreateModal not Execution yet. Modal is Empty');
        }
        $btnsave = '';
        foreach ($attrBtnSave as $key => $val) {
            $btnsave .= "$key='$val' ";
        }

        $this->modal .='</div>
                <div class="modal-footer">
                    <div id="msg" class=""></div>
                    <button type="button"  data-dismiss="modal" aria-hidden="true" class="btn btn-sm btn-white">Close</button>
                    <button class="btn btn-sm btn-success" ' . $btnsave . '>Simpan</button>
                </div>
            </form>
        </div>
    </div>
</div>';
        return $this->modal;
    }

    /**
     * 
     * @param String $option 'cd' = Create Delete , 'edt' = Edit
     * @return boolean
     */
    function priv_user($option) {
        $hak = $this->session->userdata('spg_hak');
        if ($this->session->userdata('spg_type') == 0) {
            return TRUE;
        }
        $return = FALSE;
        switch ($option) {
            case 'cd': // create delete
                $return = ($hak == 1 || $hak == 3);
                break;
            case 'edt': // edt
                $return = ($hak == 2 || $hak == 3);
                break;
        }
        return $return;
    }

    /**
     * Konversi tanggal insert db
     * @param type $tanggal format d-m-Y
     */
    function convtanggal($tanggal) {
        if ($tanggal == '') {
            return NULL;
        } else {
            return date('Y-m-d', strtotime($tanggal));
        }
    }

    /**
     * 
     * @param String $jenis nama jenis list yg akan ditmpilkan, or FALSE. Jika $jenis FALSE return array all list with list column as index
     * @param String $select String selected value.
     * @param boolean $iskey Default <b>FALSE</b>. jika array mengandung key assosiatif. 
     * @param boolean $returnArray Default <b>FALSE</b>. jika kembalian ingin berupa array. <b>TRUE</b> Otherwise. 
     * @return string
     */
    function opt_list($jenis = FALSE, $select = '', $iskey = false, $returnArray = false) {
        if (!$jenis) {
            $data = $this->gData->list_column('spg_list', 'list_id')->result_array();
            $return = array();
            foreach ($data as $v) {
                $jns = $v['jenis'];
                $return[$jns] = json_decode($v['list'], TRUE);
            }
            return $return;
        } else {
            $data = $this->gData->get_data('spg_list', array('jenis' => $jenis))->row_array();
            $li = json_decode($data['list'], TRUE);
            if ($returnArray) {
                return $li;
            } else {
                $opt = $this->set_optlist($li, $select, $iskey);
                return $opt;
            }
        }
    }

    function set_optlist($array, $selectedValue = '', $iskey = false) {
        $opt = '';
        foreach ($array as $key => $val) {
            $v = ($iskey) ? $key : $val;
            $sel = ($selectedValue == $v) ? 'selected=""' : '';
            $opt .= '<option ' . $sel . ' value="' . $v . '">' . $val . '</option>';
        }
        return $opt;
    }

    /**
     * 
     * @param String $jenis kolom mket_jns dari table spg_mket. dipisah dengan koma jika ingin menampilkan lebih dari satu jenis
     * @param Numeric $selectId selected Id Mket 
     * @param Boolean $returnArray default FALSE. True return array 2 dimension with index in array : id, ket, value
     * @return array dengan index dari masing2 $jenis yang diinputkan
     */
    function opt_mket($jenis, $selectId = '', $returnArray = false) {
        $a = explode(',', $jenis);
        $b = array_filter($a);
        $s = $this->gData->list_column('spg_mket', 'mket_jns', '', FALSE);
        $data = $s->where_in('mket_jns', $b)->get();
        if ($returnArray) {
            $r = array();
            foreach ($data->result_array() as $v) {
                $r[$v['mket_jns']][] = array('id' => $v['mket_id'], 'ket' => $v['mket_keterangan'], 'value' => $v['mket_value']);
            }
            return $r;
        } else {
            $opt = array();
            foreach ($data->result_array() as $key => $v) {
                if (!isset($opt[$v['mket_jns']])) {
                    $opt[$v['mket_jns']] = '';
                }
                $sel = ($selectId == $v['mket_id']) ? 'selected=""' : '';
                $opt[$v['mket_jns']] .= '<option ' . $sel . ' value="' . $v['mket_id'] . '">' . $v['mket_keterangan'] . '</option>';
            }
            return $opt;
        }
    }

    /**
     * Menyimpan log login user
     * @param String $option 'lgn' or 'lgt'
     */
    function log_lgn($option) {
        $aksi = ($option == 'lgn' ? '<b class="text-success">Login</b>' :
                        '<b class="text-danger">Logout</b>' );
        if ($this->session->userdata('spg_type') == 2) {
            $data['log_nip'] = $this->session->userdata('spg_nip');
        }
        $data['log_timestamp'] = date('Y-m-d h:i:s');
        $data['log_username'] = $this->session->userdata('spg_username');
        $data['log_id'] = md5($data['log_username'] . $data['log_timestamp']);
        $data['log_aksi'] = $aksi;
        $this->gData->save_data('spg_loglogin', $data);
    }

}
