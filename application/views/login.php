<!DOCTYPE html>
<html lang="en">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="description" content="" />
        <meta name="keywords" content="" />
        <title>Sevima Test</title>
        <link rel="icon" href="images/fav.png" type="image/png" sizes="16x16"> 
        
        <link rel="stylesheet" href="<?= config_item('css') ?>main.min.css">
        <link rel="stylesheet" href="<?= config_item('css') ?>style.css">
        <link rel="stylesheet" href="<?= config_item('css') ?>color.css">
        <link rel="stylesheet" href="<?= config_item('css') ?>responsive.css">

        <!-- ================== BEGIN BASE JS ================== -->
        <script src="<?= config_item('plugin') ?>pace/pace.min.js"></script>
        <!-- ================== END BASE JS ================== -->

    </head>
    <body>
        <!--<div class="se-pre-con"></div>-->
        <div class="theme-layout">
            <div class="container-fluid pdng0">
                <div class="row merged">
                    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                        <div class="land-featurearea">
                            <div class="land-meta">
                                <h1>Sevima Test</h1>
                                <p>
                                    project by Akhmad Yusuf Fadli
                                </p>
                                <div class="friend-logo">
                                    <span><img src="<?= base_url('public/user/profil_5.jpg') ?>" style="border-radius: 50%; width: 30%;" alt=""></span>
                                </div>
                            </div>	
                        </div>
                    </div>
                    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                        <div class="login-reg-bg">
                            <div class="log-reg-area sign">
                                <h2 class="log-title">Login</h2>
                                    <p id="notif"></p>
                                <form id="form-login">
                                    <div class="form-group">	
                                        <input type="text" id="input" name="username" required="required"/>
                                        <label class="control-label" for="input">Username</label><i class="mtrl-select"></i>
                                    </div>
                                    <div class="form-group">	
                                    <input type="password" name="password" required="required"/>
                                    <label class="control-label" for="input">Password</label><i class="mtrl-select"></i>
                                    </div>
                                    <!-- <a href="#" title="" class="forgot-pwd">Forgot Password?</a> -->
                                    <div class="submit-btns">
                                        <button class="mtr-btn btn-signin" type="button"><span>Login</span></button>
                                        <button class="mtr-btn signup" type="button"><span>Register</span></button>
                                    </div>
                                </form>
                            </div>
                            <div class="log-reg-area reg">
                                <h2 class="log-title">Register</h2>
                                    <p id="signup-msg"></p>
                                <form id="form-signup">
                                    <div class="form-group">	
                                    <input type="text" name="nama" required="required"/>
                                    <label class="control-label" for="input">Nama</label><i class="mtrl-select"></i>
                                    </div>
                                    <div class="form-group">	
                                    <input type="text" name="username" required="required"/>
                                    <label class="control-label" for="input">User Name</label><i class="mtrl-select"></i>
                                    </div>
                                    <div class="form-group">	
                                    <input type="password" name="password" required="required"/>
                                    <label class="control-label" for="input">Password</label><i class="mtrl-select"></i>
                                    </div>
                                    
                                    <a href="#" title="" class="already-have">Already have an account</a>
                                    <div class="submit-btns">
                                        <button class="mtr-btn btn-signup" type="button"><span>Register</span></button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
	
        <!-- ================== BEGIN BASE JS ================== -->
        <!-- <script data-cfasync="false" src="../../cdn-cgi/scripts/5c5dd728/cloudflare-static/email-decode.min.js"></script> -->
        <script src="<?= config_item('js') ?>main.min.js"></script>
        <script src="<?= config_item('js') ?>script.js"></script>
        <script src="<?= config_item('js') ?>gmsPlugin.js"></script>
        <!-- ================== END BASE JS ================== -->

        <script>
            $(document).ready(function () {
                
            }).on('click', '.btn-signup', function() {
                $('#form-signup').trigger('submit');
            }).on('submit', '#form-signup', function (e) {
                e.preventDefault();
                var fo = $(this), i = fo.find('button[type="submit"]').find('i'), paneMsg = $('#signup-msg');
                // if (fo.find('input[type=text]').val() == '') {
                //     paneMsg.removeClass().addClass('alert alert-danger').html('Username SKP tidak boleh kosong');
                //     return;
                // } else if (fo.find('input[type=password]').val() == '') {
                //     paneMsg.removeClass().addClass('alert alert-danger').html('Password SKP tidak boleh kosong');
                //     return;
                // }

                gAjax(i, {
                    url: '<?= base_url('login/doSignUp') ?>',
                    data: fo.serializeArray(),
                    dataType: 'JSON',
                    done: function (e) {
                        if (e.ind == 1) {
                            paneMsg.removeClass().addClass('alert alert-success').html(e.msg);
                            setTimeout(function () {
                                // window.location.reload(true);
                                $('.already-have').trigger('click');
                            }, 1300);
                        } else {
                            paneMsg.removeClass().addClass('alert alert-danger').html(e.msg);
                            setTimeout(function () {
                                paneMsg.removeClass().html('');
                            }, 5000);
                        }
                    }
                });
            }).on('hidden.bs.modal', '#modal-sign-up', function () {
                $(this).find('form')[0].reset();
                $(this).find('.signup-msg').removeClass().addClass('signup-msg');
            }).on('click', '.btn-signin', function() {
                $('#form-login').trigger('submit');
            }).on('submit', '#form-login', function (e) {
                e.preventDefault();
                var fo = $(this), i = fo.find('button[type="submit"]').find('i'), paneMsg = $('#notif');
                gAjax(i, {
                    url: '<?= base_url('login/doLogin') ?>',
                    data: fo.serializeArray(),
                    dataType: 'JSON',
                    done: function (e) {
                        if (e.ind == 1) {
                            paneMsg.removeClass().addClass('alert alert-success').html(e.msg);
                            setTimeout(function () {
                                paneMsg.removeClass().html('');
                            }, 5000);
                            window.location.reload();
                        } else {
                            paneMsg.removeClass().addClass('alert alert-danger').html(e.msg);
                            setTimeout(function () {
                                paneMsg.removeClass().html('');
                            }, 5000);
                        }
                    }
                });
            });
        </script>
    </body>
</html>
