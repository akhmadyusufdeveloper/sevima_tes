<script>
$(document).ready(function(){
  
}).on('submit', '#form-search', function (e) {
    e.preventDefault();
    var f = $(this);
    var dt = f.serializeArray();

    gAjax('', {
        url: '<?= base_url('friends/cari') ?>',
        data: dt,
        done: function (e) {
            if (isJSON(e)) {
                var jsn = $.parseJSON(e);
                $('#list_follow').html(jsn.list_search);
            } else {
                msgAlert(jsn.msg);
            }
        }
    });
}).on('click', '.btn-friend', function() {
    gAjax('', {
        url: '<?= base_url('friends/follow') ?>/' + $(this).data('id_user') + '/' + $(this).data('action'),
        done: function (e) {
            if (isJSON(e)) {
                var jsn = $.parseJSON(e);
                if (jsn.sts == 1) {
                    msgSuccess(jsn.msg);
                    window.location.reload();
                } else {
                    msgAlert(jsn.msg);
                }
            } else {
                msgAlert(jsn.msg);
            }
        }
    });
});

</script>