<script>
$(document).ready(function(){
  
}).on('submit', '#fo-post', function (e) {
    e.preventDefault();
    var f = $(this);
    var dt = new FormData(this);

    gAjax('', {
        url: '<?= base_url('dashboard/save_post') ?>',
        data: dt,
        contentType: false,
        processData: false,
        done: function (e) {
            if (isJSON(e)) {
                var jsn = $.parseJSON(e);
                if (jsn.sts == 1) {
                    msgSuccess(jsn.msg);
                    // $('.loadMore').html(jsn.list_feed);
                    window.location.reload();
                } else {
                    msgAlert(jsn.msg);
                }
            } else {
                msgAlert(jsn.msg);
                setTimeout(function () {
                    $('body').addClass('modal-open')
                }, 500);
            }
        }
    });
}).on('click', '.btn-comment', function() {
    $(this).parents('.comment-post').trigger('submit');
}).on('submit', '.comment-post', function (e) {
    e.preventDefault();
    var f = $(this);
    var dt = f.serializeArray();

    gAjax('', {
        url: '<?= base_url('dashboard/save_comment') ?>',
        data: dt,
        done: function (e) {
            if (isJSON(e)) {
                var jsn = $.parseJSON(e);
                if (jsn.sts == 1) {
                    // msgSuccess(jsn.msg);
                    $('.loadMore').html(jsn.list_feed);
                } else {
                    msgAlert(jsn.msg);
                }
            } else {
                msgAlert(jsn.msg);
            }
        }
    });
}).on('click', '.like', function() {
    let like = $(this);
    gAjax('', {
        url: '<?= base_url('dashboard/like') ?>/' + $(this).data('id_post'),
        done: function (e) {
            if (isJSON(e)) {
                var jsn = $.parseJSON(e);
                if (jsn.sts == 1) {
                    // msgSuccess(jsn.msg);
                    like.find('i').removeClass();
                    like.find('i').addClass(jsn.icon_like);
                    like.find('ins').html(jsn.count_like);
                } else {
                    msgAlert(jsn.msg);
                }
            } else {
                msgAlert(jsn.msg);
            }
        }
    });
}).on('change', '#file_path', function() {
  readURL(this);
}).on('click', '.btn-del-post', function () {
    var b = $(this), i = b.find('i'), id = b.data('id_post');
    bootbox.confirm("Apakah anda akan menghapus post berikut ?", function (vars) {
        if (vars) {
            gAjax(i, {
                url: '<?= base_url('dashboard/del_post') ?>/' + id,
                done: function (s) {
                    if (isJSON(s)) {
                        var ss = $.parseJSON(s);
                        if (ss.sts == 1) {
                            msgSuccess(ss.msg);
                            $('.loadMore').html(ss.list_feed);
                        } else {
                            msgAlert(ss.msg);
                        }
                    } else {
                        msgAlert('Terjadi kesalahan ketika parsing data', 'Error Message');
                    }
                }
            });
        }
    });
}).on('click', '.btn-del-comment', function () {
    var b = $(this), i = b.find('i'), id = b.data('id_comment');
    bootbox.confirm("Apakah anda akan menghapus comment berikut ?", function (vars) {
        if (vars) {
            gAjax(i, {
                url: '<?= base_url('dashboard/del_comment') ?>/' + id,
                done: function (s) {
                    if (isJSON(s)) {
                        var ss = $.parseJSON(s);
                        if (ss.sts == 1) {
                            msgSuccess(ss.msg);
                            $('.loadMore').html(ss.list_feed);
                        } else {
                            msgAlert(ss.msg);
                        }
                    } else {
                        msgAlert('Terjadi kesalahan ketika parsing data', 'Error Message');
                    }
                }
            });
        }
    });
});

function readURL(input) {
  if (input.files && input.files[0]) {
    var reader = new FileReader();
    
    reader.onload = function(e) {
      $('#preview_img_post').attr('src', e.target.result);
    }
    
    reader.readAsDataURL(input.files[0]); // convert to base64 string
  }
}
</script>