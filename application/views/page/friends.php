<div class="col-lg-3"></div>
<div class="col-lg-6 col-lg-offset-3">
    <div class="central-meta">
        <div class="frnds">
            <div>
                <form id="form-search">
                    <div class="form-group">
                        <div class="row">
                            <div class="col-10">
                                <input type="text" name="value_search" placeholder="Search Friend" style="border-bottom: 1px solid #a2a2a2;">
                            </div>
                            <div class="col-2 text-right">
                                <button data-ripple=""><i class="ti-search"></i></button>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
            <ul class="nearby-contct" id="list_follow">
                <?= $list_follow ?>
            </ul>
        </div>
    </div>