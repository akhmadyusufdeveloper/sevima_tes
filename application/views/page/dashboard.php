<div class="col-lg-3"></div>
<div class="col-lg-6 col-lg-offset-3">
    <div class="central-meta">
        <div class="new-postbox">
            <img id="preview_img_post" src="" alt="" style="margin-bottom:15px;">
            <figure>
                <img src="<?= $user_img ?>" alt="">
            </figure>
            <div class="newpst-input">
                <form id="fo-post">
                    <textarea rows="2" name="caption" placeholder="write something"></textarea>
                    <div class="attachments">
                        <ul>
                            <li>
                                <i class="fa fa-image"></i>
                                <label class="fileContainer">
                                    <input type="file" name="file_path" id="file_path">
                                </label>
                            </li>
                            <li>
                                <button type="submit">Post</button>
                            </li>
                        </ul>
                    </div>
                </form>
            </div>
        </div>
    </div><!-- add post new box -->
    <div class="loadMore">
        <?= $list_feed ?>
    </div>
</div><!-- centerl meta -->