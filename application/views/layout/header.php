<!DOCTYPE html>
<html lang="en">

<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta name="description" content="" />
	<meta name="keywords" content="" />
	<meta content="<?= base_url() ?>" name="url" />
	<title>Sevima Test</title>
	<link rel="icon" href="images/fav.png" type="image/png" sizes="16x16">

	<link rel="stylesheet" href="<?= config_item('css') ?>main.min.css">
	<link rel="stylesheet" href="<?= config_item('css') ?>style.css">
	<link rel="stylesheet" href="<?= config_item('css') ?>color.css">
	<link rel="stylesheet" href="<?= config_item('css') ?>responsive.css">

	<link href="<?= config_item('plugin') ?>gritter/css/jquery.gritter.css" rel="stylesheet" id="theme" />

	<?php
	if (isset($css)) {
		foreach ($css as $cs) {
			echo '<link rel="stylesheet" href="' . $cs . '.css" type="text/css"/>';
		}
	}
	?>

	<script src="<?= config_item('plugin') ?>pace/pace.min.js"></script>

</head>

<body>
	<!--<div class="se-pre-con"></div>-->
	<div class="theme-layout">

		<section>
			<div class="feature-photo">
				<figure><img src="<?= config_item('assets').'img/resources/timeline-1.jpg' ?>" alt=""></figure>
				<div class="add-btn">
					<span><?= $followers ?> followers</span>
					<a style="<?= $disabled_follow ? 'display:none;' : '' ?>" href="#" title="" data-ripple="">Follow</a>
				</div>
				<form id="form-img-profil" class="edit-phto">
					<i class="fa fa-camera-retro"></i>
					<label class="fileContainer">
						Edit Photo Profil
					<input type="file" name="img-profil" id="img-profil">
					</label>
				</form>
				<div class="container-fluid">
					<div class="row merged">
						<div class="col-lg-2 col-sm-3">
							<div class="user-avatar">
								<figure>
									<img src="<?= $user_img ?>" alt="">
									<form class="edit-phto">
										<i class="fa fa-camera-retro"></i>
										<label class="fileContainer">
											Edit Display Photo
											<input type="file">
										</label>
									</form>
								</figure>
							</div>
						</div>
						<div class="col-lg-10 col-sm-9">
							<div class="timeline-info">
								<ul>
									<li class="admin-name">
									  <h5><?= $namauser ?></h5>
									  <span><?= $username ?></span>
									</li>
									<li>
										<a class="<?= ($module == 'dashboard') ? 'active' : '' ?>" href="<?= base_url() ?>" title="" data-ripple="">Time line</a>
										<a class="<?= ($module == 'friends') ? 'active' : '' ?>" href="<?= base_url('friends') ?>" title="" data-ripple="">Friends</a>
										<a class="" href="<?= base_url('login/logout') ?>" title="" data-ripple="">Logout</a>
									</li>
								</ul>
							</div>
						</div>
					</div>
				</div>
			</div>
		</section>

		<section>
			<div class="gap gray-bg">
				<div class="container-fluid">
					<div class="row">
						<div class="col-lg-12">
							<div class="row" id="page-contents">



		