                        </div>	
					</div>
				</div>
			</div>
		</div>	
	</section>

<!-- ================== BEGIN PAGE LEVEL JS ================== -->
<!-- <script data-cfasync="false" src="../../cdn-cgi/scripts/5c5dd728/cloudflare-static/email-decode.min.js"></script> -->
<script src="<?= config_item('js') ?>main.min.js"></script>
<script src="<?= config_item('js') ?>script.js"></script>
<script src="<?= config_item('plugin') ?>gritter/js/jquery.gritter.js"></script> 
<script src="<?= config_item('js') ?>gmsPlugin.js"></script>
<script src="<?= config_item('plugin') ?>notification/bootbox.min.js"></script>
<!-- ================== END PAGE LEVEL JS ================== -->
<?php
if (isset($jscript)) {
    foreach ($jscript as $js) {
        echo '<script src="' . $js . '.js"></script>';
    }
}
?>

<?php
if (isset($page_js)) {
    echo $page_js;
}
?>

<script>
    $(document).ready(function(){
  
    }).on('change', '#img-profil', function() {
        $('#form-img-profil').trigger('submit');
    }).on('submit', '#form-img-profil', function(e) {
        e.preventDefault();
        var f = $(this);
        var dt = new FormData(this);

        gAjax('', {
            url: '<?= base_url('dashboard/profil_image') ?>',
            data: dt,
            contentType: false,
            processData: false,
            done: function (e) {
                if (isJSON(e)) {
                    var jsn = $.parseJSON(e);
                    if (jsn.sts == 1) {
                        msgSuccess(jsn.msg);
                        // $('.loadMore').html(jsn.list_feed);
                        window.location.reload();
                    } else {
                        msgAlert(jsn.msg);
                    }
                } else {
                    msgAlert(jsn.msg);
                    setTimeout(function () {
                        $('body').addClass('modal-open')
                    }, 500);
                }
            }
        });
    });
</script>
    
</body>	

</html>