<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Tokenmodel extends CI_Model {

    function token_isactive($id_user) {
        $dt = $this->db->get_where('user', array('id_user' => $id_user))->row_array();
        $now = date("Y-m-d H:i:s");
        if (strtotime($now) <= strtotime($dt['token_expired'])) {
            $sql = $this->db->where('id_user', $id_user)
                    ->update('user', array('token_expired' => date("Y-m-d H:i:s", strtotime('+2 hours'))));
            return $sql;
        } else {
            return FALSE;
        }
    }

}