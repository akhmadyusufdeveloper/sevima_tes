<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class M_user extends CI_Model {

    function get_user($username) {
        $user = $this->db->get_where('user', array('username' => $username));

        $result['get'] = FALSE;
        $result['username'] = '';
        if ($user->num_rows() > 0) {
            $return['get'] = TRUE;

            $data_user = $user->row_array();
            $result = array_merge($result, $data_user);
        }
        return $result;
    }

    function get_data_user($id_user) {
        return $this->db->get_where('user', array('id_user' => $id_user))->row_array();
    }

    function register($data) {
        return $this->db->insert('user', $data);
    }

    function update($data, $id_user) {
        $this->db->where('id_user', $id_user);
        return $this->db->update('user', $data);
    }

    function authentication($username, $password) {
        $cek = $this->db->get_where('user', array('username' => $username, 'password' => $password));

        $result = array(
            'status' => false,
            'message' => 'Login gagal'
        );
        if ($cek->num_rows() == 1) {
            $dt = $cek->row_array();
            $result = array(
                'status' => TRUE,
                'message' => 'Login berhasil',
                'data' => array(
                    'id_user' => $dt['id_user']
                )
            );
        }
        return $result;
    }

    function token_register($id_user, $token) {
        return $this->db->where('id_user', $id_user)->update('user', array('token' => $token, 'token_expired' => date("Y-m-d H:i:s", strtotime('+2 hours'))));
    }




    function laporan($pelapor_id) {
        $berita = $this->db->order_by('report_datetime', 'DESC')->limit(4)->get_where('cnd_berita', array('pelapor_id' => $pelapor_id));
        $data = array();
        foreach ($berita->result_array() as $b) {
            switch ($b['status']) {
                case 0:
                    $b['status'] = 'Proses';
                    break;
                case 1:
                    $b['status'] = 'Publish';
                    break;
                default:
                    $b['status'] = 'Unpublish';
                    break;
            }
            $b['berita_img'] = explode(',', $b['berita_img']);
            
            $data[] = $b;
        }
        return $data;
    }

    function berita($pelapor_id) {
        $berita = $this->db->select('berita.*, pelapor.nama AS nama_pelapor')
                            ->from('cnd_berita berita')
                            ->join('cnd_pelapor pelapor', 'pelapor.pelapor_id = berita.pelapor_id')
                            ->where(array('berita.pelapor_id !=' => $pelapor_id, 'berita.status' => 1))
                            ->order_by('publish_datetime', 'DESC')
                            ->limit(3)
                            ->get();
                            
        $data = array();
        foreach ($berita->result_array() as $b) {
            $b['status'] = 'Publish';
            $b['berita_img'] = explode(',', $b['berita_img']);
            $data[] = $b;
        }
        return $data;
    }

}
    